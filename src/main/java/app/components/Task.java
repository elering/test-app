package app.components;

import app.entity.User;
import app.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class Task {

    private static final Logger log = LoggerFactory.getLogger(Task.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Autowired
    private UserRepository userRepository;

    @Scheduled(fixedRate = 5000)
    @Transactional
    public void reportCurrentTime() throws InterruptedException {
        long c = userRepository.count();
        log.info("1: The time is now {} and user count {}",
                dateFormat.format(new Date()), c);
        insertData(c);
        Thread.sleep(1000);
    }

    @Scheduled(fixedRate = 5000)
    @Transactional
    public void reportCurrentTime3() throws InterruptedException {
        log.info("2: The time is now {} and user count {}",
                dateFormat.format(new Date()), userRepository.count());
        Thread.sleep(500);
    }

    @Scheduled(fixedRate = 5000)
    @Transactional
    public void reportCurrentTimeAgain() throws InterruptedException {
        long c = userRepository.count();
        log.info("3: The time is now {} and user count {}",
                dateFormat.format(new Date()), c);
        insertData(c);
        Thread.sleep(1000);
    }

    void insertData(long c) {
        int n = 0;
        while (c < 10000 && n++ < 100) {
            User u = new User();
            u.setEmail("user@test.ee");
            u.setName("Name " + System.nanoTime());
            userRepository.save(u);
        }
    }
}
