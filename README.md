Juhend
===

1) Paigalda etteantud virtuaalmasinasse selles git repositooriumis olev java rakendus

* Rakenduse lähtekoodi saab git'i abil kloonida virtuaalmasinasse aadressilt: http://kasutaja@git.elering.sise/elering-it-osakond/test-app.git
* Rakendus tuleb kompileerida Maveni abil war pakiks, kasutades "production" profiili
* Virtuaalmasinasse on juba paigaldatud mysql server
    - Kasutajanimi: root
    - Parool: root
* Rakenduse "production" profiili conf asub failis src/main/resources/application-production.properties
* Mysql serverisse tuleb luua "production" profiilis olev kasutaja koos parooliga ning andmebaas    
* War pakk tuleb paigaldada Tomcati serversisse
* Paigaldatud tomcati rakendusele tuleb ette panna apache2 reverse proxy, mis avaneb pordilt 80
* Virtuaalmasinasse ei ole paigaldatud maven'it, tomcati serverit, apache2'te ega java't - need tuleb paigaldada ise

Boonus
---
1. Lisa apache2 ka https (port 443) reverse proxy (ssl termination)
2. Suuna kõik http (80) peale tulnud päringud https pordile
3. Loo https proxile ise uus sertifikaat
    * Riik: EE
    * Maakond: Hrjumaa
    * Linn: Tallinn
    * Organisatsioon: Elering
    * Osakond: IT
    * Common name: Serveri IP
4. Keela virtuaalmasina tasemel väljaspoolt ligipääs pordile 8080


Abi ja vihjed:
---
* Virtuaalmasina saab käivitada desktopil oleva WM Player'i abil
  - Vali AppClean image ja vajuta "Power on" nuppu
* Virtuaalmasina IP saab käsuga: ifconfig
* Virtuaalmasina, arvuti, gitlabsi (kasutada standard login'it) ligipääs:
    - Kasutajanimi: kasutaja
    - Parool: Parool12
* Desktopile on paigaldatud Putty, mille abil saab virtuaalmasinale ligi ssh kaudu, kasutades masina IP'd
* Desktopile on paigaldatud Mysql Workbench  